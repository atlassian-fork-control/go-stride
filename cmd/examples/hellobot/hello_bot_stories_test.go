package main

import (
	"testing"

	"bitbucket.org/atlassian/go-stride/pkg/mock_stride"
	"bitbucket.org/atlassian/go-stride/pkg/stride"
	"github.com/golang/mock/gomock"
)

func Test_run_story(t *testing.T) {

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mock := mock_stride.NewMockClient(ctrl)
	mock.EXPECT().ValidateToken("jwt-token").Return(nil, nil)

	story := mock_stride.StrideStory{
		MessagesIN: []mock_stride.Inbound{
			mock_stride.NewInbound("bob", "/bot-mention", stride.ParagraphDocument("Hello")),
		},
		MessagesOUT: []mock_stride.Outbound{
			mock_stride.NewOutbound(stride.ParagraphDocument("Hey, what's up? (Sorry, that's all I can do)")),
		},
	}

	mock_stride.RunStories(t, mock, buildHandler(&WebApp{client: mock}), story)
}
