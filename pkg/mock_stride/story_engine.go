package mock_stride

import (
	"bitbucket.org/atlassian/go-stride/pkg/stride"
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

type Inbound struct {
	Url            string
	CloudID        string
	ConversationID string
	Sender         string
	Msg            *stride.Payload
}

type Outbound struct {
	CloudID        string
	ConversationID string
	Msg            *stride.Payload
	destIsUser     bool
}

type StrideStory struct {
	MessagesIN  []Inbound
	MessagesOUT []Outbound // payload output
}

func httpEncodeMessage(msg Inbound) *http.Request {
	structuredMessage := stride.Message{
		CloudID: msg.CloudID,
		Conversation: struct {
			ID string `json:"id"`
		}{ID: msg.ConversationID},
		Sender: struct {
			ID string `json:"id"`
		}{ID: msg.Sender},
		Message: struct {
			ID   string `json:"id"`
			Text string `json:"text"`
		}{ID: "MSG_ID"},
	}
	text, _ := json.Marshal(msg.Msg)
	structuredMessage.Message.Text = string(text)

	encoding, err := json.Marshal(structuredMessage)
	buf := &bytes.Buffer{}
	buf.Write(encoding)
	req, err := http.NewRequest("POST", msg.Url, buf)
	req.Header.Add("Authorization", "Bearer jwt-token")
	if err != nil {
		panic(err)
	}
	return req
}

func NewInbound(sender, url string, payload *stride.Payload) Inbound {
	return Inbound{
		Url:            url,
		CloudID:        "MY_CLOUD",
		ConversationID: "MY_ROOM",
		Sender:         sender,
		Msg:            payload,
	}
}

func NewOutbound(payload *stride.Payload) Outbound {
	return Outbound{
		CloudID:        "MY_CLOUD",
		ConversationID: "MY_ROOM",
		Msg:            payload,
		destIsUser:     false,
	}
}

func (x Outbound) ToUser(user string) Outbound {
	x.destIsUser = true
	x.ConversationID = user
	return x
}

func RunStories(t *testing.T, mock *MockClient, handler http.Handler, story StrideStory) {
	for _, msgOut := range story.MessagesOUT {
		if msgOut.destIsUser {
			mock.EXPECT().SendUserMessage(msgOut.CloudID, msgOut.ConversationID, msgOut.Msg).Return(nil)
		} else {
			mock.EXPECT().SendMessage(msgOut.CloudID, msgOut.ConversationID, msgOut.Msg).Return(nil)
		}
	}

	for _, msgIN := range story.MessagesIN {
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, httpEncodeMessage(msgIN))
		resp := w.Result()
		assert.Equal(t, 200, resp.StatusCode)
	}
}
