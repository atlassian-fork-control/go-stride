package stride

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAuth(t *testing.T) {
	testCases := []struct {
		name     string
		respData string
		err      error
	}{
		{
			name: "valid response",
			respData: `{
                "access_token": "zzzzzzzzzzzzzzzzzz",
                "expires_in": 3600,
                "scope": "participate:conversation manage:conversation",
                "token_type": "Bearer"
            }`,
			err: nil,
		}, {
			name: "missing access_token",
			respData: `{
                "expires_in": 3600,
                "scope": "participate:conversation manage:conversation",
                "token_type": "Bearer"
            }`,
			err: errors.New("invalid auth token: missing access_token in auth response"),
		}, {
			name: "missing expires_in",
			respData: `{
                "access_token": "zzzzzzzzzzzzzzzzzz",
                "scope": "participate:conversation manage:conversation",
                "token_type": "Bearer"
            }`,
			err: errors.New("invalid auth token: missing expires_in in auth response"),
		}, {
			name: "missing scope",
			respData: `{
                "access_token": "zzzzzzzzzzzzzzzzzz",
                "expires_in": 3600,
                "token_type": "Bearer"
            }`,
			err: errors.New("invalid auth token: missing scope in auth response"),
		}, {
			name: "missing token_type",
			respData: `{
                "access_token": "zzzzzzzzzzzzzzzzzz",
                "expires_in": 3600,
                "scope": "participate:conversation manage:conversation"
            }`,
			err: errors.New("invalid auth token: missing token_type in auth response"),
		},
	}
	for _, tc := range testCases {
		t.Logf("Running case %s", tc.name)
		mux := http.NewServeMux()
		server := httptest.NewServer(mux)

		mux.HandleFunc("/oauth/token", func(w http.ResponseWriter, r *http.Request) {
			if r.URL.String() != "/oauth/token" {
				t.Errorf("Incorrect URL = %v, want %v", r.URL, "/oauth/token")
			}
			w.Header().Add("Content-Type", "application/json")
			fmt.Fprintf(w, "%s", tc.respData)
		})

		client := &clientImpl{
			mutex:          &sync.Mutex{},
			apiBaseURL:     server.URL,
			authAPIBaseURL: server.URL,
			httpClient:     http.DefaultClient,
		}
		_, err := client.GetAccessToken()
		server.Close()
		assert.Equal(t, tc.err, err)
	}
}
